
if ($.fn.pagination){
    $.fn.pagination.defaults.beforePageText = '第';
    $.fn.pagination.defaults.afterPageText = '共{pages}页';
    $.fn.pagination.defaults.displayMsg = '显示{from}到{to},共{total}记录';
}
var createGridHeaderContextMenu = function(e, field) {
    e.preventDefault();
    var grid = $(this);
    var headerContextMenu = this.headerContextMenu;
    if (!headerContextMenu) {
        var tmenu = $('<div style="width:100px;"></div>').appendTo('body');
        var fields = grid.datagrid('getColumnFields');
        for (var i = 0; i < fields.length; i++) {
            var col = grid.datagrid('getColumnOption', fields[i]);
            if (!col.hidden) {
                $('<div iconCls="fa fa-check-square-o" field="' + fields[i] + '"/>').html(col.title).appendTo(tmenu);
            } else {
                $('<div iconCls="fa fa-square-o" field="' + fields[i] + '"/>').html(col.title).appendTo(tmenu);
            }
        }
        //菜单内容获取
        headerContextMenu = this.headerContextMenu = tmenu.menu({
            onClick: function(item) {
                var field = $(item.target).attr('field');
                if (item.iconCls == 'fa fa-check-square-o') {
                    grid.datagrid('hideColumn', field);
                    $(this).menu('setIcon', {
                        target: item.target,
                        iconCls: 'fa fa-square-o'
                    });
                } else {
                    grid.datagrid('showColumn', field);
                    $(this).menu('setIcon', {
                        target: item.target,
                        iconCls: 'fa fa-check-square-o'
                    });
                }
            }
        });
    }
    headerContextMenu.menu('show', {
        left: e.pageX,
        top: e.pageY,
    });
};
var onBeforeExpand = function(row) {
    var _this = $(this),gridOpts = _this.treegrid('options');
    gridOpts.url = gridOpts.expandUrl.replace(/{id}/i, row.id);
}
var onDblClickRow = function(index,row) {
    var _this = $(this),opts = _this.datagrid('options');
    if (typeof opts.callback == 'function') {
        eval(opts.callback());
        return false;
    }else{
        if (row === undefined || row === null) {
            row = index;
        }
        var params = {};
        if (opts.callback instanceof Object) {
            if (opts.callback instanceof Array) {
                for (var i in opts.callback) {
                    params = $.grid.paramData(row,opts.callback[i]);
                    $.comboFuns[opts.callback[i].method](opts.callback[i],params);
                }
            }else{
                params = $.grid.paramData(row,opts.callback);
                $.comboFuns[opts.callback.method](opts.callback,params);
            }
        }
    }
    return false;
}
if ($.fn.datagrid){
    var init = {
        method:'post',
        rownumbers:true,
        pagination:true,
        remoteSort:true,
        multiSort:true,
        singleSelect:true,
        pageSize:50,
        pageList:[50,100,300,500],
        checkOnSelect:false,
        // selectOnCheck:false,
        autoRowHeight:false,
        striped:true,
        width:'100%',
        height:'100%',
        onHeaderContextMenu:createGridHeaderContextMenu,
        onDblClickRow:onDblClickRow,
    };
    $.extend($.fn.datagrid.defaults,init);
}
if ($.fn.treegrid){
    var init = {
        method:'post',
        rownumbers:true,
        pagination:true,
        remoteSort:true,
        multiSort:true,
        singleSelect:true,
        pageSize:50,
        pageList:[50,100,300,500],
        checkOnSelect:false,
        // selectOnCheck:false,
        autoRowHeight:false,
        striped:true,
        height:'100%',
        onHeaderContextMenu:createGridHeaderContextMenu,
        onBeforeExpand:onBeforeExpand,
        onDblClickRow:onDblClickRow,
    };
    $.extend($.fn.treegrid.defaults,init);
}
if ($.messager){
    $.messager.defaults.ok = '<span class="btn btn-sm btn-primary">确定</span>';
    $.messager.defaults.cancel = '<span class="btn btn-sm btn-default">取消</span>';
    $.messager.show.title = '提示消息';
}
$.map(['validatebox','textbox','passwordbox','filebox','searchbox',
        'combo','combobox','combogrid','combotree',
        'datebox','datetimebox','numberbox',
        'spinner','numberspinner','timespinner','datetimespinner','tagbox','combotreegrid'], function(plugin){
    if ($.fn[plugin]){
        $.fn[plugin].defaults.missingMessage = '此为必填项';
        if ($.fn[plugin].defaults.onClickClearIcon) {
            $.fn[plugin].defaults.onClickClearIcon = function(arg) {
                $(arg.data.target)[plugin]('clear');
            }
        }
        if ($.fn[plugin].defaults.tipPosition !== undefined) {
            $.fn[plugin].defaults.tipPosition = 'top';
        }
    }
});
$.map(['datagrid','treegrid','combogrid','combotreegrid','window','panel','dialog',],function(plugin) {
    if ($.fn[plugin]){
        if ($.fn[plugin].defaults.loadingMessage !== undefined) {
            $.fn[plugin].defaults.loadingMessage = '正在处理，请稍待. . .';
        }
        if ($.fn[plugin].defaults.loadMsg !== undefined) {
            $.fn[plugin].defaults.loadMsg = '正在处理，请稍待. . .';
        }
    }
})
if ($.fn.form) {
    $.fn.form.defaults.iframe = true;
}
if ($.fn.validatebox){
    var defRulesMsg = {
        email:'不是有效的邮箱地址',
        url:'不是有效的URL地址',
        length:'请输入{0}~{1}长度的内容',
        remote:'请修正该字段',
    };
    for (var i in defRulesMsg) {
        $.fn.validatebox.defaults.rules[i].message = defRulesMsg[i];
    }
}
if ($.fn.calendar){
    $.fn.calendar.defaults.firstDay = 1;
    $.fn.calendar.defaults.weeks = ['日','一','二','三','四','五','六'];
    $.fn.calendar.defaults.months = ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'];
}
if ($.fn.datebox){
    $.fn.datebox.defaults.tipPosition = 'top';
    $.fn.datebox.defaults.currentText = '今天';
    $.fn.datebox.defaults.closeText = '关闭';
    $.fn.datebox.defaults.okText = '确定';
    $.fn.datebox.defaults.formatter = function(date){
        var _this = $(this),opts = _this.datebox('options');
        return $.utils.dateFormat(date,opts.format);
    };
    $.fn.datebox.defaults.parser = function(s){
        if (!s) return new Date();
        var ss = s.split('-');
        var y = parseInt(ss[0],10);
        var m = parseInt(ss[1],10);
        var d = parseInt(ss[2],10);
        if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
            return new Date(y,m-1,d);
        } else {
            return new Date();
        }
    };
}
if ($.fn.datetimebox && $.fn.datebox){
    $.extend($.fn.datetimebox.defaults,{
        firstDay:1,
        currentText: $.fn.datebox.defaults.currentText,
        closeText: $.fn.datebox.defaults.closeText,
        okText: $.fn.datebox.defaults.okText
    });
}
if ($.fn.datetimespinner){
    $.fn.datetimespinner.defaults.selections = [[0,4],[5,7],[8,10],[11,13],[14,16],[17,19]]
}

if ($.fn.window){
    var init = {
        modal:true,
        minimizable:false,
        collapsible:false,
        maximizable:true,
        maximized:false,
        minimized:true,
        width:'55%',
        height:'50%',
        cache:false,
        zIndex:199010,
    };
    $.extend($.fn.window.defaults,init);
}

if ($.fn.dialog){
    var init = {
        minimizable:false,
        collapsible:false,
        maximizable:true,
        maximized:false,
        minimized:true,
        width:'35%',
        height:'50%',
        cache:false,
    };
    $.extend($.fn.dialog.defaults,init);
}

if ($.fn.combogrid) {
    var init = {
        method:'post',
        rownumbers:true,
        pagination:true,
        remoteSort:true,
        multiSort:true,
        singleSelect:true,
        pageSize:50,
        pageList:[50,100,300,500],
        checkOnSelect:true,
        selectOnCheck:true,
        autoRowHeight:false,
        striped:true,
        onHeaderContextMenu:createGridHeaderContextMenu,
        onBeforeExpand:onBeforeExpand,
        filter:function(q, row){
            var opts = $(this).combogrid('options');
            return row[opts.textField].indexOf(q) == 0;
        },
    };
    $.extend($.fn.combogrid.defaults,init);
    $.fn.combogrid.defaults.onShowPanel = function(){
        $(this).combogrid('panel').find('td[field=progress] .gprogressbar').each(function(index, el) {
            if ($(el).is(":empty")) {
                $(el).progressbar();
            }
        });
    }
    $.fn.combogrid.defaults.onHidePanel = function() {
        var opts = $(this).combogrid('options');
        if (opts.uploadurl !== undefined) {
            $('div.'+$(this).attr('t')).remove();
        }
    }
}
if ($.fn.combotreegrid) {
    var init = {
        method:'post',
        rownumbers:true,
        pagination:true,
        remoteSort:true,
        multiSort:true,
        singleSelect:true,
        pageSize:50,
        pageList:[50,100,300,500],
        checkOnSelect:true,
        selectOnCheck:true,
        autoRowHeight:false,
        striped:true,
        onHeaderContextMenu:createGridHeaderContextMenu,
        onBeforeExpand:onBeforeExpand,
    };
    $.extend($.fn.combotreegrid.defaults,init);
}
if ($.fn.filebox) {
    $.fn.filebox.defaults.buttonText = '浏览';
    $.fn.filebox.defaults.prompt = '请选择上传文件. . .';
}
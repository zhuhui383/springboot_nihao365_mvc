/**
 * 页面模块js
 * @authors zhuhui (zhuhui@yidian-mall.com)
 * @date    2019-07-03 17:02:16
 * @version $Id$
 */
$(function() {
    var Objs = {
        load: function(){
            Objs.loadResize();
        },
        loadResize: function(){
        },
        jQ:function(str){
            if (parent !== null && window.frames.length != parent.frames.length){
                return (str === undefined || str === '')?$(window.parent.document):$(window.parent.document).find(str);
            }else{
                return (str === undefined || str === '')?$(document):$('html').find(str);
            }
        },
    };
    // 合并对象到jQuery
    $.extend({
        page:{
            load:Objs.load,
            loadResize:Objs.loadResize,
        },
    });
    // 执行默认加载函数
    $.page.load();
    $(window).resize(function() {
        $.page.loadResize();
    });
});
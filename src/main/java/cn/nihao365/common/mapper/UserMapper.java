package cn.nihao365.common.mapper;

import cn.nihao365.common.core.SuperMapper;
import cn.nihao365.common.model.Users;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户Mapper
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-10-28 22:04:24
 */
@Mapper
public interface UserMapper extends SuperMapper<Users>{

    /**
     * 获取一个用户信息
     * @param id userID
     * @return 用户信息
     */
    Users getUserById(Integer id);
}

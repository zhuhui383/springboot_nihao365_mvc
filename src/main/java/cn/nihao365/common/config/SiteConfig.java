package cn.nihao365.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-11-10 23:47:36
 */
@Component
@ConfigurationProperties(prefix = "site",ignoreInvalidFields = true)
@PropertySource(value = "classpath:config/site.properties",ignoreResourceNotFound = true,encoding = "UTF-8")
@Data
public class SiteConfig{
    private String sysName;
    private String sysDescription;
    private String sysKeywords;
    private String sysBaseUrl;

}

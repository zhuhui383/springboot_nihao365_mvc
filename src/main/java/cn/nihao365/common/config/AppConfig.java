package cn.nihao365.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-11-10 23:47:36
 */
@Component
@ConfigurationProperties(prefix = "app",ignoreInvalidFields = true)
@PropertySource(value = "classpath:config/application-bootstrap.properties",ignoreResourceNotFound = true,encoding = "UTF-8")
@Data
public class AppConfig{
    private String applicationName;
    private int serverPort;
    private String dbUrl;
    private String dbUserName;
    private String redisDatabase;
    private String emailUserName;
    private String maxFileSize;
    private String maxRequestSize;
    private String uploadLocation = "upload";
}

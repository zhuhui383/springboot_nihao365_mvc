package cn.nihao365.common.core;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;


/**
 * 自定义 mapper 父类
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-11-08 21:23:52
 */
public interface SuperMapper<T> extends BaseMapper<T>{

    // 这里可以写自己的公共方法、注意不要让 mp 扫描到误以为是实体 Base 的操作
}

package cn.nihao365.common.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-11-08 23:03:47
 */
@Controller
@RequestMapping("/admin")
public class AdminController extends CommonController{
}

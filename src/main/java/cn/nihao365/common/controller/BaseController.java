package cn.nihao365.common.controller;

import org.springframework.stereotype.Controller;

/**
 * 基类控制器
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-11-08 23:06:06
 */
@Controller
public class BaseController{

    // 在这里可以写一下基础方法

    public BaseController(){
        System.out.println("111111111");
    }
}

package cn.nihao365.common.component;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-11-11 00:58:50
 */
@Component
@Data
@Accessors(chain = true)
public class ExcelComponent{

    private String message = "";
    public static ExcelComponent Util;

    @PostConstruct
    public void init(){
        Util = this;
    }

    private boolean export(){
        // TODO: 2021/11/13 导出数据Excel文件
        return true;
    }
}

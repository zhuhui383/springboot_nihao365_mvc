package cn.nihao365.common.component;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * session组件
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-11-09 22:30:12
 */
@Component
@Data
@Accessors(chain = true)
public class SessionComponent{
    private int expire = 0;
    private String message = "";
    public static SessionComponent Util;

    @PostConstruct
    public void init(){
        Util = this;
    }


}

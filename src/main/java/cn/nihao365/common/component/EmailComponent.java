package cn.nihao365.common.component;

import cn.hutool.core.util.ArrayUtil;
import cn.nihao365.common.config.AppConfig;
import lombok.Data;
import lombok.experimental.Accessors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * 邮件组件
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-11-09 22:00:29
 */
@Component
@Data
@Accessors(chain = true)
public class EmailComponent{
    @Autowired
    JavaMailSenderImpl mailSender;
    @Autowired
    private AppConfig appConfig;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    public static EmailComponent Util;

    private String subject = "邮件";
    private String text = "内容";
    private List<String> toAccount;
    private List<String> ccAccount;
    private List<String> bccAccount;
    private List<String> attachmentFile;

    @PostConstruct
    public void init(){
        Util = this;
    }

    /**
     * 发送邮件
     * @return 是否发送成功
     */
    public boolean send(){
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = null;
        try{
            // 构建一个邮件对象
            helper = new MimeMessageHelper(message, true);
            // 设置邮件发送日期
            helper.setSentDate(new Date());
            // 设置邮件的主题
            helper.setSubject(this.subject);
            // 设置邮件的正文
            helper.setText(this.text);

            // 设置邮件发送者
            helper.setFrom(appConfig.getEmailUserName());

            // 设置邮件接收者，可以有多个接收者，中间用逗号隔开，以下类似
            // message.setTo("65*****72@qq.com","11****83*qq.com");
            helper.setTo(ArrayUtil.toArray(this.toAccount,String.class));

            // 设置邮件抄送人，可以有多个抄送人
            if(ArrayUtil.isNotEmpty(this.ccAccount)){
                helper.setCc(ArrayUtil.toArray(this.ccAccount,String.class));
            }

            // 设置隐秘抄送人，可以有多个
            if(ArrayUtil.isNotEmpty(this.bccAccount)){
                helper.setBcc(ArrayUtil.toArray(this.bccAccount,String.class));
            }

            //验证文件数据是否为空
            String [] fileArray = ArrayUtil.toArray(this.attachmentFile,String.class);
            if(ArrayUtil.isNotEmpty(fileArray)){
                for(String s: fileArray){
                    //添加附件
                    FileSystemResource file = new FileSystemResource(s);
                    if (!file.exists()) {
                        logger.info("外部配置不存在。");
                    }else {
                        helper.addAttachment(s.substring(s.lastIndexOf(File.separator)),file);
                    }

                }
            }

            mailSender.send(message);
            //日志信息
            logger.info("邮件已经发送。");
        }catch(MessagingException e){
            logger.error("发送邮件时发生异常！", e);
            return false;
        }
        return true;
    }

}

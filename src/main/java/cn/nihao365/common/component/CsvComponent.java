package cn.nihao365.common.component;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-11-11 00:59:26
 */
@Component
@Data
@Accessors(chain = true)
public class CsvComponent{

    private String message = "";
    public static CsvComponent Util;

    @PostConstruct
    public void init(){
        Util = this;
    }

    private boolean export(){
        // TODO: 2021/11/13 导出CSV数据文件
        // 考虑使用 hutool 的文件工具类 CsvUtil
        //https://www.hutool.cn/docs/#/core/文本操作/CSV文件处理工具-CsvUtil
        return true;
    }
}

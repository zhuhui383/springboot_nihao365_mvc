package cn.nihao365.common.component;

import org.springframework.stereotype.Component;

/**
 * 文件组件
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-11-11 00:56:44
 */
@Component
public class FileComponent{
    // TODO: 2021/11/13 编写对文件的操作：复制 移动 重命名 删除；
    //  对文件夹的操作：创建 复制 移动 重命名 删除

    //考虑使用 hutool 的文件工具类 FileUtil
    //https://www.hutool.cn/docs/#/core/IO/文件工具类-FileUtil


}

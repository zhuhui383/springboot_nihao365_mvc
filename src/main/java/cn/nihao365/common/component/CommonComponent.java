package cn.nihao365.common.component;

import org.springframework.stereotype.Component;
import java.util.Random;

/**
 * 公共组件
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-11-09 22:30:39
 */
@Component
public class CommonComponent{

    /**
     * 获取微秒
     * @return 微秒
     */
    public static Long getMicTime() {
        // 微秒
        long cuTime = System.currentTimeMillis() * 1000;
        // 纳秒
        long nanoTime = System.nanoTime();
        return cuTime + (nanoTime - nanoTime / 1000000 * 1000000) / 1000;
    }

    public static String microTimeString(){
        Long ll = getMicTime();
        String pre = (ll+"").substring(0, 10);
        String suf = (ll+"").replace(pre,"");
        return suf+"000_"+pre;
    }

    /**
     * length用户要求产生字符串的长度
     * @param length 字符串长度
     * @return 随机字符串
     */
    public static String getRandomString(int length){
        String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<length;i++){
            int number=random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }







}

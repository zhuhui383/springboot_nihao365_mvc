package cn.nihao365.common.model.enums;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.EnumValue;

/**
 * 必须先配置该包扫描自动注入，查看mybatis-plus.type-enums-package参数
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-11-08 22:04:24
 */
public enum TypeEnum{
    /**
     * 常用
     */
    DISABLED(0, "禁用"),
    NORMAL(1, "正常");
    @EnumValue
    private final int value;
    private final String desc;

    TypeEnum(final int value, final String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Serializable getValue() {
        return this.value;
    }

    /**
     * Jackson 注解为 JsonValue 返回中文 json 描述
     * @return 返回
     */
    public String getDesc() {
        return this.desc;
    }
}

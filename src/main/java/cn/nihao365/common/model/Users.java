package cn.nihao365.common.model;

import cn.nihao365.common.core.SuperEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * users 表模型
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-10-28 21:59:51
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("users")
public class Users extends SuperEntity{

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("userNo")
    private String userNo;

    @TableField("userEmail")
    private String userEmail;

    @TableField("userName")
    private String userName;

    @TableField("userNickName")
    private String userNickName;

    @TableField("userPwd")
    private String userPwd;

    @TableField("isManage")
    private Integer isManage;

    @TableField("isDisabled")
    private Integer isDisabled;

    @TableField("roleNo")
    private String roleNo;

    @TableField("userAvatar")
    private String userAvatar;

    @TableField("createTime")
    private Integer createTime;

    @TableField("updateTime")
    private Integer updateTime;

    @TableField("rememberToken")
    private String rememberToken;

    @TableField("userNote")
    private String userNote;

}

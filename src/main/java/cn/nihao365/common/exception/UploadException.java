package cn.nihao365.common.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

/**
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-11-14 13:56:57
 */
@ControllerAdvice
public class UploadException{
    @ResponseBody
    @ResponseStatus
    @ExceptionHandler(Exception.class)
    public String handlerMethodArgumentException(Exception e){
        if(e instanceof MaxUploadSizeExceededException){
            return "文件过大";
        }
        return "错误信息："+e.getMessage();
    }
}

package cn.nihao365.common.utils;

import org.springframework.stereotype.Component;

/**
 * 封装Json工具类
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-11-08 21:59:51
 */
public class JsonResultUtil{
    private boolean success;
    private String status;
    private String msg;
    private Object obj;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }
}

package cn.nihao365.common.utils;

import org.springframework.stereotype.Component;
import java.util.regex.Pattern;

/**
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-11-15 00:56:43
 */
@Component
public class ValidatorUtil{

    /**
     * 手机号
     */
    private static final String REGEX_MOBILE = "^1([34578])\\d{9}$";

    /**
     * 邮箱
     */
    private static final String REGEX_EMAIL = "^(\\w-*\\.*)+@(\\w-?)+(\\.\\w{2,})+$";

    /**
     * 密码：8~16位包含英文大小写和数组
     */
    private static final String REGEX_PASSWORD = "^(?=.*?[a-zA-Z])(?=.*?\\d).{8,16}$";

    /**
     * 验证码：8位数值
     */
    private static final String REGEX_CAPTCHA = "^\\d{8}$";

    /**
     * 用户名 不能包含特殊字符
     */
    private static final String REGEX_USER_NAME = "^[a-zA-Z]\\w{5,20}$";

    /**
     * 正整数
     */
    private static final String REGEX_INT = "^[1-9]\\d*$";

    /**
     * 匹配非负整数（正整数 + 0）
     */
    private static final String REGEX_INT0 = "^[1-9]\\d*|0$";

    /**
     * 匹配正浮点数
     */
    private static final String REGEX_FLOAT = "^[1-9]\\d*\\.\\d*|0\\.\\d*[1-9]\\d*$";

    /**
     * 匹配非负浮点数（正浮点数 + 0）
     */
    private static final String REGEX_FLOAT0 = "^[1-9]\\d*\\.\\d*|0\\.\\d*[1-9]\\d*|0?\\.0+|0$";

    /**
     * URL
     */
    private static final String REGEX_URL = "^((ht|f)tps?)://([\\w-]+(\\.[\\w-]+)*/?)+(\\?([\\w-.,@?^=%&:/~+#]*)+)?$";

    /**
     * 正则表达式：验证汉字
     */
    private static final String REGEX_CHINESE = "^[\u4e00-\u9fa5],{0,}$";

    /**
     * 正则表达式：验证身份证
     */
    private static final String REGEX_ID_CARD = "(^\\d{18}$)|(^\\d{15}$)";

    /**
     * 正则表达式：验证IP地址
     */
    private static final String REGEX_IP_ADDR = "(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)";

    /**
     * 校验手机号
     * @param mobile 手机号
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isMobile(String mobile) {
        return Pattern.matches(REGEX_MOBILE, mobile);
    }

    /**
     * 校验邮箱
     * @param email 邮箱
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isEmail(String email) {
        return Pattern.matches(REGEX_EMAIL, email);
    }

    /**
     * 校验密码
     * @param password 密码
     * @return 校验通过返回true，否则返回false
     */
    public static String isPassword(String password) {
        if(Pattern.matches(REGEX_PASSWORD, password)){
            return "";
        }
        return "密码必须8~16位的大小写字母、数字组合";
    }

    /**
     *校验验证码
     * @param captcha 验证码
     * @return 校验通过返回true，否则返回false
     */
    public static String isCaptcha(String captcha) {
        if(Pattern.matches(REGEX_CAPTCHA, captcha)){
            return "";
        }
        return "验证码必须是8位有效数字";
    }

    /**
     * 校验用户名
     * @param userName 用户名
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isUserName(String userName) {
        return Pattern.matches(REGEX_USER_NAME, userName);
    }

    /**
     * 校验整数
     * @param str 数字
     * @param isZero 是否包含 0
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isInt(String str,Boolean isZero){
        if(isZero){
            return Pattern.matches(REGEX_INT0, str);
        }else{
            return Pattern.matches(REGEX_INT, str);
        }
    }

    /**
     * 校验浮点数
     * @param str 数字
     * @param isZero 是否包含 0
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isFloat(String str,Boolean isZero){
        if(isZero){
            return Pattern.matches(REGEX_FLOAT0, str);
        }else{
            return Pattern.matches(REGEX_FLOAT, str);
        }
    }

    /**
     * 校验URL
     * @param url 地址
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isUrl(String url) {
        return Pattern.matches(REGEX_URL, url);
    }

    /**
     * 校验汉字
     * @param chinese 汉字
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isChinese(String chinese) {
        return Pattern.matches(REGEX_CHINESE, chinese);
    }

    /**
     * 校验身份证
     * @param idCard 身份证号
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isIdCard(String idCard) {
        return Pattern.matches(REGEX_ID_CARD, idCard);
    }

    /**
     * 校验IP地址
     * @param ipAddr IP地址
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isIpAddr(String ipAddr) {
        return Pattern.matches(REGEX_IP_ADDR, ipAddr);
    }

}

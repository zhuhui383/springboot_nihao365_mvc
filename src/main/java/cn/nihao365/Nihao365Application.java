package cn.nihao365;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 应用启动器
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-11-08 21:59:51
 */
@SpringBootApplication(scanBasePackages = "cn.nihao365.*")
public class Nihao365Application{

    public static void main(String[] args){
        SpringApplication.run(Nihao365Application.class,args);
    }

}

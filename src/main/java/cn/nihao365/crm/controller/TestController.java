package cn.nihao365.crm.controller;

import cn.hutool.core.util.StrUtil;
import cn.nihao365.common.component.EmailComponent;
import cn.nihao365.common.component.UploadComponent;
import cn.nihao365.common.config.AppConfig;
import cn.nihao365.common.controller.BaseController;
import cn.nihao365.common.model.Users;
import cn.nihao365.crm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 测试控制器
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-11-07 12:03:52
 */
@Controller
public class TestController extends BaseController{
    @Autowired
    private UserService userService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private AppConfig appConfig;

    @RequestMapping("/test")
    public String index(Model model){
        model.addAttribute("message","这是一个测试页面");
        return "test/index";
    }

    @GetMapping("/test/user")
    public String user(Model model) {
        Users users = userService.getUserById(12);
        model.addAttribute("user", users);
        return "test/user";
    }

    @RequestMapping("/test/upload")
    public String singleFileUpload(@RequestParam(name = "file") MultipartFile file,HttpServletRequest request){
        String filePath = UploadComponent.Util.upload(file);
        if(StrUtil.isEmpty(filePath)){
            request.setAttribute("msg","文件上传失败");
        }else{
            request.setAttribute("msg","文件上传成功");
        }
        return "test/result";
    }

    /**
     * 返回一个json数据
     * @return 接送数据
     */
    @RequestMapping("/test/json")
    @ResponseBody
    public Object json(){
        Map<Object,Object> map = new HashMap<>(2);
        map.put("name", "张三");
        map.put("age", 17);
        return map;
    }

    /**
     * 返回一个redis数据
     */
    @GetMapping("/test/redis")
    @ResponseBody
    public String redis(){
        stringRedisTemplate.opsForValue().set("name","zhu hui");
        return stringRedisTemplate.opsForValue().get("name");
    }

    /**
     * 返回一个json数据
     * @return 接送数据
     */
    @RequestMapping("/test/conf")
    @ResponseBody
    public String config(){
        System.out.println(appConfig.getApplicationName());
        System.out.println(appConfig.getEmailUserName());
        return "ok:"+new Date().toString();
    }

    /**
     * 返回一个json数据
     * @return 接送数据
     */
    @RequestMapping("/test/send")
    @ResponseBody
    public String send(){
        ArrayList<String> toAccount = new ArrayList<>();
        toAccount.add("604522472@qq.com");
        ArrayList<String> ccAccount = new ArrayList<>();
        ccAccount.add("1130341383@qq.com");
        ArrayList<String> bccAccount = new ArrayList<>();
        bccAccount.add("zhuhui@nihao365.com");

        ArrayList<String> files = new ArrayList<>();
        files.add("D:\\用户目录\\Downloads\\hhhh.txt");
        boolean isSend = EmailComponent.Util.setToAccount(toAccount)
        //boolean isSend = emailComponent.setToAccount(toAccount)
                .setSubject("zhu111hui"+new Date().toString())
                .setText("设置邮件的正文")
                //.setCcAccount(ccAccount)
                //.setBccAccount(bccAccount)
                .setAttachmentFile(files)
                .send();
        if(isSend){
            return "success";
        }
        return "error";
    }



}

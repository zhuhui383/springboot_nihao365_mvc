package cn.nihao365.crm.controller;

import cn.nihao365.common.controller.AdminController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 主页控制器
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-11-07 18:49:06
 */
@Controller
public class HomeController extends AdminController{

    @RequestMapping("/home")
    public String index(Model model){
        model.addAttribute("message","系统crm主页面，需要登录哦~");
        return "home/index";
    }

}

package cn.nihao365.crm.controller;

import cn.nihao365.common.controller.CommonController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 登录控制器
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-10-28 21:31:13
 */
@Controller
public class ForgetController extends CommonController{

    @RequestMapping(value = "/forget")
    public String index(Model model){
        model.addAttribute("message","这是一个找回密码页面");
        //System.out.println("2.----LoginController.login");
        return "forget/index";
    }

}

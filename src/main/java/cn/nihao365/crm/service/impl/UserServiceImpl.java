package cn.nihao365.crm.service.impl;

import cn.nihao365.common.mapper.UserMapper;
import cn.nihao365.common.model.Users;
import cn.nihao365.crm.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * UserService的实现
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-10-28 22:12:05
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, Users> implements UserService {

    private UserMapper userMapper;

    @Autowired(required = false)
    public UserServiceImpl(UserMapper userMapper){
        this.userMapper = userMapper;
    }

    @Override
    public Users getUserById(Integer id){
        return userMapper.getUserById(id);
    }

    @Override
    public String resultParam(String id){
        return id;
    }
}

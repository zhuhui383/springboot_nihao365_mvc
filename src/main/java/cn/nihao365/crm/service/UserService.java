package cn.nihao365.crm.service;

import cn.nihao365.common.model.Users;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

/**
 * user 业务服务层
 * @author zhu hui (zhuhui@nihao365.cn)
 * @version 1.0
 * @date 2021-10-28 22:10:41
 */
@Service
public interface UserService extends IService<Users>{

    /**
     * 通过用户ID获取用户信息
     * @param id 用户ID
     * @return 用户信息
     */
    Users getUserById(Integer id);

    /**
     * 返回参数
     * @param id 参数
     * @return 返回值
     */
    String resultParam(String id);
}
